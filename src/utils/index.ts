/* --------------------------- CODE BASE ------------------------------------ */
export const preventPaste = (event: React.SyntheticEvent) => {
  event.preventDefault();
  return false;
};

export * from './constants';
