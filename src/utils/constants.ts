/* --------------------------- CONSTANTS ------------------------------------ */
export const typingText = 'I would like to register to this course and I am ' +
  'completing the registration.';

export const errorMessage =
  'Please make sure that you input the exact same text.';
