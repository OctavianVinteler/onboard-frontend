/* ------------------------ PROJECT IMPORTS --------------------------------- */
import { post } from './utils';

/* ------------------------------ TYPES ------------------------------------- */
interface RegistrationData {
  userId: string,
  pattern1: string,
  pattern2: string,
  textId: string,
  device: string
}

interface ResultData {
  result: boolean
}

/* --------------------------- CODE BASE ------------------------------------ */
export const registrationCall = (data: RegistrationData) => {
  const { userId, pattern1, pattern2, textId, device } = data;

  return post({
    url: 'registration',
    body: {
      userId,
      pattern1,
      pattern2,
      textId,
      device
    }
  })
    .then(result => result);
}
