/* ---------------------------- IMPORTS ------------------------------------- */
import axios from 'axios';

/* --------------------------- CODE BASE ------------------------------------ */
let config = require('../config/config.json');

const { serverAddress } = config;

export const get = ({
  url,
  params
}: {
    url: string,
    params?: object
  }) =>
  axios.get(`${serverAddress}${url}`, params)
    .then(res => res.data)
    .catch((err: Error) => {
      console.log(`HTTP GET request error: ${err.message}`);
      throw err;
    });

export const post = ({
  url,
  body
}: {
    url: string,
    body?: object
  }) =>
  axios.post(`${serverAddress}${url}`, body)
    .then(res => res.data)
    .catch((err: Error) => {
      console.log(`HTTP POST request error: ${err.message}`);
      throw err;
    });
