/* ------------------------------ CSS --------------------------------------- */
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/components/app.css';

/* ---------------------------- IMPORTS ------------------------------------- */
import React from 'react';

/* ------------------------ PROJECT IMPORTS --------------------------------- */
import { Registration} from './forms/registration';

/* --------------------------- CODE BASE ------------------------------------ */
export class App extends React.Component {
  render() {
    // @ts-ignore
    const { params } = this.props.match;

    return (
      <div className="app">
        <header className="app-header">
          <p>
            Please use the form below in order to complete your course registration
          </p>
        </header>
        <body className="app-body">
          <Registration userId={params.userId}/>
        </body>
      </div>
    );
  }
}

export default App;
