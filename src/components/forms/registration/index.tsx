/* ------------------------------ CSS --------------------------------------- */
import '../../../styles/components/forms/registration.css';

/* ---------------------------- IMPORTS ------------------------------------- */
import React from 'react';
import Button from 'react-bootstrap/Button';
import { TypingDNA } from '../../../external/typingdna';

/* ------------------------ PROJECT IMPORTS --------------------------------- */
import { typingText, errorMessage } from '../../../utils';
import { preventPaste } from '../../../utils';
import { registrationCall } from '../../../callers/registration';

/* ------------------------------ TYPES ------------------------------------- */
interface UserData {
  userId: string;
}

interface StateData {
  text1: string,
  text2: string,
  text1Error: string | null,
  text2Error: string | null,
  formSubmitted: boolean,
  result: boolean,
  submitEnabled: boolean
}

/* --------------------------- CODE BASE ------------------------------------ */
export class Registration extends React.Component<UserData> {

  constructor(props : UserData) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.validateData = this.validateData.bind(this);
  }

  state : StateData = {
    text1: '',
    text2: '',
    text1Error: null,
    text2Error: null,
    formSubmitted: false,
    result: false,
    submitEnabled: true
  }

  // @ts-ignore
  tdna : any = new TypingDNA();

  validateData = () => {
    const { text1, text2 } = this.state;

    const text1Error = text1 !== typingText
      ? errorMessage
      : null;

    const text2Error = text2 !== typingText
      ? errorMessage
      : null;

    return Promise.resolve(
      this.setState({
        text1Error,
        text2Error
      })
    );
  }

  handleSubmit = (event : React.SyntheticEvent) => {
    event.preventDefault();
    const { userId } = this.props;

    return this.validateData()
    .then(() => {
      if (this.state.text1Error || this.state.text2Error) {
        return Promise.resolve();
      } else {

        this.setState({
          submitEnabled: false
        })

        const textId : string = this.tdna.getTextId(typingText);

        const pattern1 : string = this.tdna.getTypingPattern({
          type: 1,
          text: typingText,
          targetId: 'textArea1'
        });

        const pattern2 : string = this.tdna.getTypingPattern({
          type: 1,
          text: typingText,
          targetId: 'textArea2'
        });

        return registrationCall({
          userId,
          pattern1,
          pattern2,
          textId,
          device: this.tdna.isMobile() ? 'mobile' : 'desktop'
        })
        .then(({ result }) => {
          console.log('result', result);
          this.setState({
            formSubmitted: true,
            result
          });
        });
      }
    });
  }

  handleTextChange = (event : any) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value
    });
  }

  render() {
    this.tdna.addTarget('textArea1');
    this.tdna.addTarget('textArea2');

    return (
      <div>
        { !this.state.formSubmitted &&
          <div className="registration-form">
            <p className="registration-title">Insert the following text twice:</p>
            <form onSubmit={this.handleSubmit}>
              <div>
                <div className="form-group registration-typing_area">
                  <label htmlFor="textArea1">{typingText}</label>
                  <input className="form-control rounded-0 registration-text_area"
                         type="text" id="textArea1" name="text1"
                         onChange={this.handleTextChange} value={this.state.text1}
                         onPaste={preventPaste} autoComplete="off" />
                  { this.state.text1Error
                      ? <div className='registration-invalid_text'>{this.state.text1Error}</div>
                      : null
                  }
                </div>
                <div className="form-group registration-typing_area">
                  <label htmlFor="textArea2">{typingText}</label>
                  <input className="form-control rounded-0 registration-text_area"
                         type="text" id="textArea2" name="text2"
                         onChange={this.handleTextChange} value={this.state.text2}
                         onPaste={preventPaste} autoComplete="off" />
                  { this.state.text2Error
                      ? <div className='registration-invalid_text'>{this.state.text2Error}</div>
                      : null
                  }
                </div>
              </div>
              <Button id="submitButton" className="registration-submit_button" type="submit" size="lg" variant="primary" disabled={!this.state.submitEnabled}>
                Register
              </Button>
            </form>
          </div>
        }
        { this.state.formSubmitted &&
          <div className="registration-form">
            <p className="registration-title">Thank you for submitting the form!</p>
            { this.state.result &&
              <p className="registration-title">Validation successful!</p>
            }
            { !this.state.result &&
              <p className="registration-title">Validation failed!</p>
            }
          </div>
        }
      </div>

    );
  }
}
