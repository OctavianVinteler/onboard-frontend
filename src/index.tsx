/* ------------------------------ CSS --------------------------------------- */
import './styles/index.css';

/* ---------------------------- IMPORTS ------------------------------------- */
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter } from 'react-router-dom';

/* ------------------------ PROJECT IMPORTS --------------------------------- */
import App from './components/app';

/* --------------------------- CODE BASE ------------------------------------ */
const routing = (
  <BrowserRouter>
    <div>
      <Route path="/registration/:userId" component={App} />
    </div>
  </BrowserRouter>
);

ReactDOM.render(routing, document.getElementById('root'));
